<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**

     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	App\User::create([
    		'name'  	=> 'Hugo Cano Admin',
    		'email' 	=> 'admin@admin.com',
    		'password' 	=> bcrypt('123'),

    	]);

        App\User::create([
            'name'      => 'Hugo Cano Invitado',
            'email'     => 'admin2@admin.com',
            'password'  => bcrypt('123'),

        ]);

        factory(App\User::class, 2)->create();
    }
}
